var app = angular.module("app", [
	"ngRoute",
	"ngResource",
	"uiGmapgoogle-maps"
]);


app.config(["$routeProvider", "$locationProvider",
	function($routeProvider, $locationProvider) {
		$routeProvider
			.when("/", {
				templateUrl: "views/index.html",
				controller: "IndexCtrl"
			})
			.when("/login", {
				templateUrl: "views/login.html"
			})
			.when("/register", {
				templateUrl: "views/register.html"
			})
			.when("/me", {
				templateUrl: "views/profile/me.html",
				controller: "ProfileCtrl"
			})
			.when("/user/:id", {
				templateUrl: "views/profile/user.html",
				controller: "UserProfileCtrl"
			})
			.when("/admin", {
				templateUrl: "views/admin/dashboard.html",
				controller: "AdminCtrl"
			})
			.otherwise({
				templateUrl: "views/404.html"
			});

		$locationProvider.html5Mode(true);

	}
]);