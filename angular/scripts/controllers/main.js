
angular.module("app").controller("MainCtrl", ["$scope", "$location", "$interval", "Auth", function($scope, $location, $interval, Auth) {

	$scope.fetchUser = function() {
		return Auth.fetchUser().then(function(data) {
			$scope.user = data;
			$scope.user.place_ids = [];
			$scope.user.search_ids = [];
			// TODO: Make more efficient, return array from API
			for(var i = 0; i < data.places.length; i++) {
				$scope.user.place_ids.push(data.places[i].place_id);
			}
			for(var i = 0; i < data.searches.length; i++) {
				$scope.user.search_ids.push(data.searches[i].place_id);
			}
			delete $scope.user.places;
			delete $scope.user.searches;
		});
	}

	$scope.fetchUser(); //TODO: Inherit in child controllers so only loading user in one place
	$interval(function() {
		$scope.fetchUser();
	}, 2500);

	$(document).ready(function() {
		$("#navbar button").click(function() {
			var href = $(this).attr("id");
			console.log(href);
			$location.path(href);
		});
	});

	$scope.logout = function() {
		Auth.logout().then(function() {
			$scope.user = {};
			$location.path("/");
		});
	}

}]);