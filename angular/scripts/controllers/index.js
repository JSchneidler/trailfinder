
angular.module('app').controller('IndexCtrl', ['$scope', '$interval', '$http', 'uiGmapGoogleMapApi', 'Places', 'Searches', function($scope, $interval, $http, uiGmapGoogleMapApi, Places, Searches) {

	document.title = 'Search | TrailFinder';

	$scope.loading = true;
	$scope.processing = false;
	$scope.error = false;
	$scope.keyword = 'park';
	$scope.radius = 1000;
	
	var Autocomplete = new google.maps.places.Autocomplete(document.getElementById('placeInput'), {});
	var PlacesService = new google.maps.places.PlacesService(document.getElementById('placesContainer'));

	function log(obj) {
		console.log(obj);
	}

	uiGmapGoogleMapApi.then(function(maps) {
		$scope.map = {
			center: { latitude: 0, longitude: 0 },
			zoom: 2,
			pan: true,
			control: {},
			options: {
				mapTypeId: google.maps.MapTypeId.HYBRID
			}
		};
		$scope.googleVersion = maps.version;
		maps.visualRefresh = true;

		$scope.centerMap = function(locationObj) {
			$scope.map.control.refresh({ latitude: locationObj.lat(), longitude: locationObj.lng() });
			$scope.map.zoom = 13;
		}

		$scope.fitBounds = function(geometry) {
			if (geometry.viewport) {
				$scope.map.control.getGMap().fitBounds(geometry.viewport);
			} else {
				$scope.centerMap(geometry.location);
			}
		}

		$scope.setActivePlace = function(place) {
			$scope.activePlace = {
				id: place.place_id,
				coords: {
					latitude: place.geometry.location.lat(),
					longitude: place.geometry.location.lng()
				},
				options: {
					label: place.name,
					place: place,
					title: place.name
				}
			}
			$scope.fitBounds(place.geometry);
		}

		$scope.loadPlace = function(index) {
			$scope.placeLoading = true;
			var id = $scope.places[index].place_id;
			PlacesService.getDetails({ placeId: id }, function(place, status) {
				log(place);
				if (status == google.maps.places.PlacesServiceStatus.OK) {
					$scope.$apply(function() {
						$scope.place = place;
						if ($scope.place.photos) {
							$scope.place.coverPhoto = $scope.place.photos[0].getUrl({ 'maxHeight': 200 });
						}
						else {
							var key = 'AIzaSyC9NbFRJCALeLaKjGZlrhFbQcRotv7egbs';
							var center = $scope.place.geometry.location.lat()+','+$scope.place.geometry.location.lng();
							var url = 'https://maps.googleapis.com/maps/api/staticmap?center='+center+'&key='+key+'&size=200x200&zoom=15&maptype=hybrid&format=png32';
							$scope.place.coverPhoto = url;
						}
					});
				} else {
					$scope.$apply(function() {
						$scope.error = true;
						console.log('Place Error: ' + status);
					});
				}
			});
			$scope.placeLoading = false;
		}

		$scope.submit = function() {
			$scope.index = 0;
			$scope.processing = true;
			$scope.error = false;
			$scope.originPlace = Autocomplete.getPlace();
			$scope.originPlace.radius = $scope.radius;
			$scope.originPlace.keyword = $scope.keyword;
			log($scope.originPlace);
			$scope.setActivePlace($scope.originPlace);
			var radius = $scope.radius;
			if (radius == 0) {
				radius = 9999;
			}
			PlacesService.radarSearch({ location: $scope.originPlace.geometry.location, type: $scope.keyword, radius: radius*1609 }, function(places, status) {
				if (status == google.maps.places.PlacesServiceStatus.OK) {
					$scope.$apply(function() {
						$scope.places = places;
						$scope.loadPlace(0);
						$scope.processing = false;
					});
				} else {
					$scope.$apply(function() {
						$scope.error = true;
						$scope.processing = false;
						console.log('Radar Error: ' + status);
					});
				}
			});
		}
		
		$scope.savePlace = function() {
			Places.save({
				'place_id': $scope.place['place_id'],
				'name': $scope.place['name'],
				'address': $scope.place['formatted_address']
			}, function(response) {
				console.log(response);
				$scope.fetchUser(); //Re-fetch user to get updated array of saved places
				//TODO: Update view, also anything else after place is saved
			});
		}
		
		$scope.saveSearch = function() {
			Searches.save({
				'place_id': $scope.originPlace['place_id'],
				'name': $scope.originPlace['name'],
				'address': $scope.originPlace['formatted_address'],
				'radius': $scope.originPlace['radius'],
				'keyword': $scope.originPlace['keyword']
			}, function(response) {
				console.log(response);
				$scope.fetchUser();
				//TODO: Update view, also anything else after search is saved
			});
		}

		$scope.prevPage = function() {
			if ($scope.index > 0) {
				$scope.index--;
				$scope.loadPlace($scope.index);
			}
		}

		$scope.firstPage = function() {
			if ($scope.index > 0 ) {
				$scope.index = 0;
				$scope.loadPlace($scope.index);
			}
		}

		$scope.nextPage = function() {
			if ($scope.index < $scope.places.length) {
				$scope.index++;
				$scope.loadPlace($scope.index);
			}
		}

		$scope.lastPage = function() {
			if ($scope.index < $scope.places.length) {
				$scope.index = $scope.places.length-1;
				$scope.loadPlace($scope.index);
			}
		}

		$scope.loading = false;
		
	});

}]);