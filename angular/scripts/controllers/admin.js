angular.module("app").controller("AdminCtrl", ["$scope", "$window", "$location", "User", "Auth", function($scope, $window, $location, User, Auth) {

    $scope.loading = true;

    //TODO: Set currentUser to $scope.user, but only after its loaded. This is redundantly loading another user object.
    $scope.currentUser = Auth.fetchUser().then(function(user) {
        if (user === null || user.role !== "Admin") {
            $window.location = "/login";
        } else {
            $scope.currentUser = user;
            $scope.users = User.GetUsers();
            $scope.viewing = "main";

            $scope.openUser = function(id) {
                User.GetUser({id: id}).$promise.then(function(user) {
                    $scope.person = user;
                    $scope.viewing = "user";
                });
            }

            $scope.deleteUser = function(id) {
                //TODO: Do validation in back-end?
                if ($scope.user.id !== id) {
                    User.DeleteUser({id: id}).$promise.then(function() {
                        $scope.users = User.GetUsers();
                        $location.path("admin");
                    });
                }
            }

            $scope.close = function() {
                $scope.viewing = "main";
            }
        }

        $scope.loading = false;
    });

}]);