angular.module('app').controller('ProfileCtrl', ['$scope', '$location', 'Places', 'Searches', 'uiGmapGoogleMapApi', function($scope, $location, Places, Searches, uiGmapGoogleMapApi) {

    $scope.viewing = 'main';
    $scope.places = Places.GetPlaces();
    $scope.searches = Searches.GetSearches();

    PlacesService = new google.maps.places.PlacesService(document.getElementById('placesContainer'));

    uiGmapGoogleMapApi.then(function(maps) {
        $scope.map = {
            center: { latitude: 0, longitude: 0 },
            zoom: 2,
            pan: true,
            control: {},
            options: {
                mapTypeId: google.maps.MapTypeId.HYBRID
            }
        };
        $scope.googleVersion = maps.version;
        maps.visualRefresh = true;

        $scope.centerMap = function(locationObj) {
            $scope.map.control.refresh({ latitude: locationObj.lat(), longitude: locationObj.lng() });
            $scope.map.zoom = 13;
        }

        $scope.fitBounds = function(geometry) {
            if (geometry.viewport) {
                $scope.map.control.getGMap().fitBounds(geometry.viewport);
            } else {
                $scope.centerMap(geometry.location);
            }
        }

        $scope.setActivePlace = function(place) {
            $scope.activePlace = {
                id: place.place_id,
                coords: {
                    latitude: place.geometry.location.lat(),
                    longitude: place.geometry.location.lng()
                },
                options: {
                    label: place.name,
                    place: place,
                    title: place.name
                }
            }
            $scope.fitBounds(place.geometry);
        }

        $scope.loadPlace = function(place_id) {
            PlacesService.getDetails({ placeId: place_id }, function(place, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    $scope.$apply(function() {
                        $scope.place = place;
                        $scope.setActivePlace(place);
                        if (place.photos) {
                            $scope.place.coverPhoto = place.photos[0].getUrl({ 'maxHeight': 200 });
                        }
                        else {
                            var key = 'AIzaSyC9NbFRJCALeLaKjGZlrhFbQcRotv7egbs';
                            var center = $scope.place.geometry.location.lat()+','+$scope.place.geometry.location.lng();
                            var url = 'https://maps.googleapis.com/maps/api/staticmap?center='+center+'&key='+key+'&size=200x200&zoom=15&maptype=hybrid&format=png32';
                            $scope.place.coverPhoto = url;
                        }
                    });
                } else {
                    $scope.$apply(function() {
                        $scope.error = true;
                        console.log('Place Error: ' + status);
                    });
                }
            });
            $scope.placeLoading = false;
        }

    });

    $scope.openPlace = function(id) {
        $scope.placeLoading = true;
        Places.GetPlace({id: id}).$promise.then(function(place) {
            $scope.viewing = 'place';
            $scope.loadPlace(place.place_id);
        });
    }

    $scope.deletePlace = function(id) {
        $scope.updatingPlaces = true;
        Places.DeletePlace({id: id}).$promise.then(function() {
            $scope.places = Places.GetPlaces();
            $scope.updatingPlaces = false;
            $scope.viewing = 'main';
        });
    }

    $scope.openSearch = function(id) {
        Searches.GetSearch({id: id}).$promise.then(function(search) {
            $scope.search = search;
            $scope.viewing = 'search';
        });
    }

    $scope.deleteSearch = function(id) {
        $scope.updatingSearches = true;
        Searches.DeleteSearch({id: id}).$promise.then(function() {
            $scope.searches = Searches.GetSearches();
            $scope.updatingSearches = false;
            $scope.viewing = 'main';
        });
    }

    $scope.close = function() {
        $scope.viewing = 'main';
    }
    
}]);