angular.module('app').factory('User', ['$resource',
    function($resource) {
        $resource('api/users/:id', {}, {
            GetUsers: { method: 'GET', isArray: true },
            GetPlacesForUserById: { method: 'GET', isArray: true, params: { id: '@id' }, url: 'api/users/:id/places'},
            GetSearchesForUserById: { method: 'GET', isArray: true, params: { id: '@id' }, url: 'api/users/:id/searches'},
            GetUser: { method: 'GET', params: { id: '@id' } },
            DeleteUser: { method: 'DELETE', params: { id: '@id' } }
        });
    }
]);