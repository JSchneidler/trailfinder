angular.module("app").factory("Places", ["$resource",
    function($resource) {
        return $resource("api/places/:id", {}, {
            GetPlaces: { method: "GET", isArray: true },
            GetPlace: { method: "GET", params: { id: "@id" } },
            DeletePlace: { method: "DELETE", params: { id: "@id" } }
        });
    }
]);

//TODO: Let $resource handle CRUD operations? http://www.sitepoint.com/creating-crud-app-minutes-angulars-resource/