angular.module("app").factory("Searches", ["$resource",
    function($resource) {
        return $resource("api/searches/:id", {}, {
            GetSearches: { method: "GET", isArray: true },
            GetSearch: { method: "GET", params: { id: "@id" } },
            DeleteSearch: { method: "DELETE", params: { id: "@id" } }
        });
    }
]);

//TODO: Let $resource handle CRUD operations? http://www.sitepoint.com/creating-crud-app-minutes-angulars-resource/