
angular.module("app").service("Auth", ["$http", function($http) { //TODO: Change to $resource implementation so can use $resolved

	var user;

	function getUser() {
		return user;
	}

	function fetchUser() {
		return $http.get("api/auth").then(function(response) {
			if (response.data.user) {
				user = response.data.user;
			} else {
				user = null;
			}
			return user;
		});
	}

	function logout() {
		return $http.get("api/auth/logout");
	}

	return {
		getUser: getUser,
		fetchUser: fetchUser,
		logout: logout
	};

}]);