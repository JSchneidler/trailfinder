<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\User as User;

class AuthController extends ApiController
{
    public function GetAuth() {
        if (Auth::check()) {
            $user = Auth::user();
            $user->places = User::find($user->id)->places()->select("place_id")->get();
            $user->searches = User::find($user->id)->searches()->select("place_id")->get();
            return $this->respondWithData([
                "user" => Auth::user()
            ]);
        }
        return $this->respondWithError("No user", 404);
    }

    public function Logout() {
        Auth::logout();
    }
}
