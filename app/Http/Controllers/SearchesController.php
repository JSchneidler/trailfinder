<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\Search as Search;

class SearchesController extends ApiController
{
    function __construct() {
        parent::__construct();
        $this->middleware('auth');
        $this->user = Auth::user();
    }

    public function index() {
        $searches = $this->user->searches;
        return $this->respondWithData($searches);
    }

    public function store(Request $request) {
        $exists = $this->user->searches()->where('place_id', $request->place_id)->exists();

        if ($exists) {
            return $this->respondWithError("Search already saved.", 200);
        }
        $search = new Search([
            'place_id' => $request->place_id,
            'name' => $request->name,
            'address' => $request->address,
            'radius' => $request->radius,
            'keyword' => $request->keyword
        ]);
        $this->user->searches()->save($search);
        return $this->respondWithSuccess('Search saved successfully.');
    }

    public function show($id) {
        $search = Search::find($id);
        if ($search) {
            return $this->respondWithData($search);
        }
        return $this->respondWithError('Search not found!', 404);
    }

    public function update($id) {
        //Empty route, cannot update a search as each is static. May change.
    }

    public function destroy($id) {
        $search = Search::find($id);
        if ($search) {
            $search->delete();
            return $this->respondWithSuccess('Search deleted');
        }
        return $this->respondWithError('Search not found!', 404);
    }
}
