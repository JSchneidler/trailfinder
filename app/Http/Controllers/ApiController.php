<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ApiController extends Controller
{
    protected $statusCode = 200;

    public function getStatusCode() {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode) {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function respond($data, $headers = []) {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithData($data, $statusCode = 200, $headers = []) {
        return $this->setStatusCode($statusCode)->respond($data, $headers);
    }

    public function respondWithError($message = "An error has occurred", $statusCode = 403, $headers = []) {
        return $this->setStatusCode($statusCode)->respond([
            "message" => $message
        ], $headers);
    }

    public function respondWithSuccess($message = "Successful operation", $statusCode = 200, $headers = []) {
        return $this->setStatusCode($statusCode)->respond([
            "message" => $message
        ], $headers);
    }

    public function respondWithMessage($message = "Server responded with a message", $statusCode = 200, $headers = []) {
        return $this->setStatusCode($statusCode)->respond([
            "message" => $message
        ], $headers);
    }
}
