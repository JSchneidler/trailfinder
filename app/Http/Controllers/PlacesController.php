<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\Place as Place;

class PlacesController extends ApiController
{
    function __construct() {
        parent::__construct();
        $this->middleware('auth');
        $this->user = Auth::user();
    }

    public function index() {
        $places = $this->user->places;
        return $this->respondWithData($places);
    }

    public function store(Request $request) {
        $exists = $this->user->places()->where('place_id', $request->place_id)->exists();

        if ($exists) {
            return $this->respondWithError("Place already saved.", 200);
        }
        $place = new Place([
            'place_id' => $request->place_id,
            'name' => $request->name,
            'address' => $request->address
        ]);
        $this->user->places()->save($place);
        return $this->respondWithSuccess('Place saved successfully.');
    }

    public function show($id) {
        $place = Place::find($id);
        if ($place) {
            return $this->respondWithData($place);
        }
        return $this->respondWithError('Place not found!', 404);
    }

    public function update($id) {
        //Empty route, cannot update a place as each is static. May change.
    }

    public function destroy($id) {
        $place = Place::find($id);
        if ($place) {
            $place->delete();
            return $this->respondWithSuccess('Place deleted');
        }
        return $this->respondWithError('Place not found!', 404);
    }
}