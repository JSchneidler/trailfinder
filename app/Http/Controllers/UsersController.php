<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User as User;

class UsersController extends ApiController
{
    function __construct() {
        $this->middleware('auth', ['only' => ['update', 'destroy']]);
    }

    public function index() {
        return $this->respondWithData(User::all());
    }

    public function show($id) {
        $user = User::find($id);
        return $this->respondWithData($user);
    }
    
    public function update($id) {
        //TODO: Update user @ PUT user/{id}
    }
    
    public function destroy($id) {
        $user = User::find($id);
        $currentUser = Auth::user();
        
        if ($currentUser->role == 'Admin' || $currentUser->id == $id) {
            $user->delete();
            return $this->respondWithSuccess('User successfully deleted');
        } else {
            return $this->respondWithError('User not deleted, failed authentication.', 401);
        }
    }
    
    public function getSearches($id) {
        $user = User::find($id);
        $searches = $user->searches;
        if ($searches) return $this->respondWithData($searches);
        else return $this->respondWithMessage("No searches for user.");
    }
    
    public function getPlaces($id) {
        $user = User::find($id);
        $places = $user->places;
        if ($places) return $this->respondWithData($places);
        else return $this->respondWithMessage("No places for user.");
    }
}
